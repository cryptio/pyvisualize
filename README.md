# pyVisualize

Audio visualization using a Raspberry Pi and LEDs

## Description
pyVisualize's creation stems from a grade 12 Production Technology project that I completed in 2019. The project consisted of a small wooden Christmas tree equipped with LEDs. These LEDS were attached to a Raspberry Pi. An IR receiver and speaker was also connected to the RPI. This allowed a user to select and play Christmas music from a playlist with the help of a remote. As the Christmas music played, the Christmas tree's LEDs would flash to the beat of the tune. The code that handles all of this originates from pyVisualize. Hi-hats, kicks, and snares get detected using frequency thresholds. This is not a fool-proof method of detecting certain beats, but from the perspective of someone who is not an audio specialist, it works fine for the use case.

## Circuit
The wiring of the Raspberry Pi is shown below using a Fritzing diagram. Since pyVisualize was used for a very specific project in my case, the number of LEDs in addition to their colors reflect that. 550 Ω of resistance is used for each LED (220 Ω followed by a 330 Ω resistor). This value was used in order to ensure that my project did not consume too much power and overload the Rasperry Pi, as I also had LEDs hooked up to the 5V pins and not just GPIO.

![](docs/pyvisualize_bb.png)

## Dependencies
The following Python modules are required:
- aubio
- pyaudio
- alsaaudio