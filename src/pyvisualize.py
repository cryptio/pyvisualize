"""Audio visualization using a Raspberry Pi and LEDs"""

__author__ = 'Chase'
__copyright__ = 'Copyright 2018, Chase'

__license__ = 'GPL'
__version__ = '0.0.1'
__maintainer__ = 'Chase'
__status__ = 'Development'

import logging
import argparse
from alsaaudio import Mixer
from gpiopin import GPIOPin
from irw import IRW
from beatdetect import BeatDetect
from ledbeatrpi import LEDBeatRPI
from typing import List


def parse_arguments():
    """
    Parses the arguments passed to the program and returns all

    :return:
    """

    parser = argparse.ArgumentParser(description="Python audio LED visualization and remote control")
    parser.add_argument('-f', '--filename', help='The filename to read a line-separated list of up to 8 songs from',
                        required=True)
    args = parser.parse_args()

    return args


def read_songs(filename: str) -> List[str]:
    """
    Read a line-separated list of song filenames into a list

    :param filename: The file containing the list of song filenames to read from
    :return: The list of songs
    """

    try:
        with open(filename) as file:
            songs = [line.rstrip('\n') for line in file]
            return songs
    except Exception as e:
        logging.fatal("Error: " + str(e))


def main():
    logging.basicConfig(level=logging.INFO)

    args = parse_arguments()

    songs_filename = args.filename
    songs = read_songs(songs_filename)
    if not 1 <= len(songs) <= 8:
        logging.fatal("The number of songs read must be between 1 and 8")

    # The GPIO pins connected to LEDs that will light up for each of beat when it is detected
    hat_pins = [
        GPIOPin(17),
        GPIOPin(24)
    ]
    snare_pins = [
        GPIOPin(27),
        GPIOPin(22)
    ]
    kick_pins = [
        GPIOPin(23),
        GPIOPin(25)
    ]

    mixer = Mixer(control='PCM', id=0)
    irw = IRW()
    ledbeatrpi = LEDBeatRPI(hat_pins, snare_pins, kick_pins)
    beatdetect = BeatDetect(callback=ledbeatrpi.beatdetect_callback)

    current_song_index = 0

    while True:
        try:

            keyname, updown = irw.next_key()  # get the key read
            logging.info('Received key:' + keyname + updown)

            # All of the keys which correspond to a song
            song_keys = {
                'KEY_0': 0,
                'KEY_1': 1,
                'KEY_2': 2,
                'KEY_3': 3,
                'KEY_4': 4,
                'KEY_5': 5,
                'KEY_6': 6,
                'KEY_7': 7,
                'KEY_8': 8,
                'KEY_9': 9
            }

            if keyname in song_keys:
                current_song_index = song_keys[keyname]
                beatdetect.play_song(songs[current_song_index])
            elif keyname == 'KEY_PREVIOUS':
                # If we are currently playing the first song and the user tries going to the previous one,
                #  wrap around to the last song
                current_song_index = (len(songs) - 1) if current_song_index == 0 else current_song_index - 1
                beatdetect.play_song(songs[current_song_index])
            elif keyname == 'KEY_NEXT':
                # If we are currently playing the last song and the user tries going to the next one,
                #   wrap around to the first song
                current_song_index = 0 if current_song_index == (len(songs) - 1) else current_song_index + 1
                beatdetect.play_song(songs[current_song_index])
            elif keyname == 'KEY_PLAYPAUSE':
                # Make sure that the stream is still a valid object
                if beatdetect.stream:
                    if beatdetect.stream.is_active():
                        beatdetect.stream.stop_stream()
                    elif beatdetect.stream.is_stopped():
                        beatdetect.stream.start_stream()
            elif keyname == 'KEY_VOLUMEUP' or keyname == 'KEY_VOLUMEDOWN':
                new_volume = mixer.getvolume()[0] + 10 if keyname == 'KEY_VOLUMEUP' else mixer.getvolume()[0] - 10

                # Only change the volume if it's a valid percentage
                if 100 >= new_volume <= 0:
                    mixer.setvolume(new_volume)
            elif keyname == 'KEY_MODE':  # EQ
                # Set the volume in the middle
                new_volume = 50
                mixer.setvolume(new_volume)
        except KeyboardInterrupt:
            logging.warning('Interrupted by user')
            break
        # Pressing a number that doesn't have a song filename mapped to it should not break the program
        except IndexError:
            pass


if __name__ == '__main__':
    main()
