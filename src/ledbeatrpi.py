"""Represents the set of LEDs hooked up to the RPI's GPIO pins that flash to the song beat"""

from datetime import datetime, timedelta
from RPi import GPIO
from beatdetect import BeatDetect
from gpiopin import GPIOPin
from typing import List


class LEDBeatRPI:

    def __init__(self, hat_pins: List[GPIOPin], snare_pins: List[GPIOPin], kick_pins: List[GPIOPin]):
        LEDBeatRPI.gpio_init()

        self.hat_pins = hat_pins
        self.snare_pins = snare_pins
        self.kick_pins = kick_pins

        # Set all of the pins to write mode
        GPIO.setup([pin.number for pin in self.hat_pins], GPIO.OUT)
        GPIO.setup([pin.number for pin in self.snare_pins], GPIO.OUT)
        GPIO.setup([pin.number for pin in self.kick_pins], GPIO.OUT)

    @staticmethod
    def gpio_init():
        """
        Initialize the GPIO settings used for this project
        :return:
        """

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)  # refer to pins by Broadcom SOC channel, not physical pin number

    @staticmethod
    def update_pin_values(pins: List[GPIOPin], value: bool):
        """
        Update a list of GPIO pins

        :param pins: The list of GPIOPin objects to update
        :param value: The value to assign and write to each pin
        :return:
        """

        for pin in pins:
            GPIO.output(pin.number, value)
            pin.value = value

    def beatdetect_callback(self, frequency: int):
        """
        Defines a callback to handle a new frequency and find out which type of beat the sound is

        :param frequency: The frequency of the current chunk
        :return:
        """

        now = datetime.now()
        # Only report a beat detection each interval in ms
        if ((self.last_detection - now).microseconds * 1000) >= self.detection_interval:
            # Determine which kind of beat the frequency falls under and light up the LEDs that correspond to it

            hat_value = BeatDetect.is_hat(frequency)
            # Don't write the exact same value to the LED every time, this is inefficient
            if self.hat_pins[0].value != hat_value:
                self.hat_count += 1
                LEDBeatRPI.update_pin_values(self.hat_pins, bool(hat_value))

            snare_value = BeatDetect.is_snare(frequency)
            if self.snare_pins[0].value != snare_value:
                self.snare_count += 1
                LEDBeatRPI.update_pin_values(self.snare_pins, bool(snare_value))

            kick_value = BeatDetect.is_kick(frequency)
            if self.kick_pins[0].value != kick_value:
                self.kick_count += 1
                LEDBeatRPI.update_pin_values(self.kick_pins, bool(kick_value))
        self.last_detection = datetime.now()

    def __del__(self):
        GPIO.cleanup()

    hat_pins = []
    snare_pins = []
    kick_pins = []

    hat_count = 0
    snare_count = 0
    kick_count = 0

    detection_interval = 300
    last_detection = datetime.now() + timedelta(milliseconds=detection_interval)
