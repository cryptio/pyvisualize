"""The following class mimics the functionality of irw"""

__author__ = 'Chase'
__copyright__ = 'Copyright 2018, Chase'

__license__ = 'GPL'
__version__ = '0.0.1'
__maintainer__ = 'Chase'
__status__ = 'Development'

import socket
from typing import Tuple


class IRW:
    def __init__(self, sockpath: str = '/var/run/lirc/lircd'):
        self.sockpath = sockpath
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        # Connect to our Unix domain socket
        self.sock.connect(sockpath)

    def next_key(self) -> Tuple[str, str]:
        """
        Get the next key pressed
        :return: tuple
        """

        while True:
            data = self.sock.recv(128)
            data = data.decode('utf8').strip()
            if data:
                break

        words = data.split()
        return words[2], words[1]

    sockpath = None
    sock = None
