"""GPIOPin represents a GPIO pin on the Raspberry pi"""

__author__ = 'Chase'
__copyright__ = 'Copyright 2018, Chase'

__license__ = 'GPL'
__version__ = '0.0.1'
__maintainer__ = 'Chase'
__status__ = 'Development'


class GPIOPin:

    def __init__(self, number: int):
        self.number = number

    # A GPIO pin has a number, which identifies it, as well as a value
    number = None
    value = None
