"""BeatDetect is used to play audio while keeping track of the current pitch and assisting in beat detection"""

__author__ = 'Chase'
__copyright__ = 'Copyright 2018, Chase'

__license__ = 'GPL'
__version__ = '0.0.1'
__maintainer__ = 'Chase'
__status__ = 'Development'

import pyaudio
from aubio import source, pitch
from typing import Callable


class BeatDetect:
    def __init__(self, callback: Callable = None):
        self.pitch_o = pitch("yin", self.win_s, self.hop_s, self.samplerate)
        self.pitch_o.set_unit("Hz")  # frequency is measured in Hz usually
        self.pitch_o.set_tolerance(self.tolerance)

        self.callback = callback

    @staticmethod
    def is_hat(frequency: int) -> bool:
        """
        Checks whether or not the specified frequency is in the range of a hi-hat
        :param frequency: int
        :return: bool
        """

        # lower = 3000
        # upper = 5000
        # https://www.musical-u.com/learn/percussion-frequencies-part-2-cymbals/
        lower = 300
        upper = 3000

        return lower <= frequency <= upper

    @staticmethod
    def is_kick(frequency: int) -> bool:
        """
        Checks whether or not the specified frequency is in the range of a kick
        :param frequency: int
        :return: bool
        """

        # http://www.moultonlabs.com/more/principles_of_multitrack_mixing_the_kick_drum_bass_relationship/P1/
        lower = 20
        upper = 100

        return lower <= frequency <= upper

    @staticmethod
    def is_snare(frequency: int) -> bool:
        """
        Checks whether or not the specified frequency is in the range of a snare
        :param frequency: int
        :return: bool
        """

        lower = 300
        upper = 600

        return lower <= frequency <= upper

    def play_song(self, filename: str):
        """
        Play the specified filename

        :param filename:
        :return:
        """

        # Cleanup  the current stream
        if self.stream and self.stream.is_active():
            self.stream.stop_stream()
            self.stream.close()

        self.audio_source = source(filename, self.samplerate, self.hop_s)
        self.samplerate = self.audio_source.samplerate

        self.stream = self.p_audio.open(format=self.pyaudio_format, channels=self.n_channels, rate=self.samplerate,
                                        output=True, frames_per_buffer=self.hop_s,
                                        stream_callback=self.pyaudio_callback)
        self.stream.start_stream()

    def pyaudio_callback(self, _in_data, _frame_count, _time_info, _status):
        """
        A custom pyaudio callback that reads the pitch of the current chunk

        :note: Note
        :param _in_data:
        :param _frame_count:
        :param _time_info:
        :param _status:
        :return:
        """

        # pitch = int(round(pitch))
        # confidence = pitch_o.get_confidence()
        # if confidence < 0.8: pitch = 0.

        samples, read = self.audio_source()
        audiobuf = samples.tobytes()

        frequency = self.pitch_o(samples)[0]
        self.total_frames += 1

        # We need a pyAudio callback to process it using aubio, then we can pass it to a user-defined callback function
        if self.callback:
            self.callback(frequency)

        if read < self.hop_s:
            return audiobuf, pyaudio.paComplete

        return audiobuf, pyaudio.paContinue

    def __del__(self):
        if self.stream:
            self.stream.close()
        self.p_audio.terminate()

    downsample = 1

    win_s = 4096 // downsample  # fft size
    hop_s = 2048 // downsample  # hop size

    tolerance = 0.8  # pitch tolerance

    pitch_o = None

    p_audio = pyaudio.PyAudio()
    pyaudio_format = pyaudio.paFloat32

    stream = None
    audio_source = None
    samplerate = 44100 // downsample  # floor division
    n_channels = 1

    total_frames = 0

    callback = None
